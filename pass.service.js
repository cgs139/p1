'use strict'

const bcrypt = require('bcrypt');

// encripta password
//
// devuelve un hash con un salt inlcuido en formato:
// 
// $2b$10$rjIiNVRtP55rcDtibqrPouYG3.vt7P8h6X0srVwXAmE/yEu6VTzo2
// ****-- *****************************************************
// Alg Cost              Salt                    Hash
function encriptaPassword( password ) {
    return bcrypt.hash( password, 10);
}

//compara password
//
// devolver vd o falso si coinciden o no el password y el hash
function comparaPassword( password, hash ){
    return bcrypt.compare( password, hash );
}

module.exports = {
    encriptaPassword,
    comparaPassword
};
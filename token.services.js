'use strict'

const jwt = require('jwt-simple');
const moment = require('moment');

const SECRET = require('../config').secret;
const EXP_TIME = require('../config').tokenExpTmp;
// crear token
//
// devuelve un otken tipo jwt 
// formato jwt:
//      header.payload.verify_signature
//
// donde:
//      header {
//              "alg": "HS256",
//              "typ": "JWT",
//
//              _id: '1234567890',
//              email: 'cgs139@gcloud.ua.es',
//              displayName: 'Celia Gomez',
//              password: 'miContraseña',
//              signUpDate: 1647968009,
//              lastLogin: 1647968009
//      }
//      payload {
//          sub: '123456789', iat: 1647968009, exp: 1647968009
//      }
//      verify_signaure = hmacsha256( base64urlencode(header)+"."+base64urlencode(payload), secret) 

function creaToken ( user ) {
    const payload = {
        sub: user._id,
        iat: moment().unix(),
        exp: moment().add(EXP_TIME, 'minutes').unix()
    };
    return jwt.encode( payload, SECRET);
}

// decodificaToken
// devuelve el identificador del usuario

function decodificaToken ( token ) {
    return new Promise( (resolve, reject) => {
        try{
            const payload = jwt.decode( token, SECRET, true);
            if(payload.exp <= moment().unix()){
                reject ({
                    status: 401,
                    message: 'el token ha caducado'
                });
                resolve( payload.sub );
            }
            console.log( payload );
            resolve( payload.sub );
        } catch {
            reject ({
                status: 500,
                message: 'El token no es valido'
            });
        }
    });
}
module.exports = {
    creaToken,
    decodificaToken
};
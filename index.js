'use strict'

const port = process.env.PORT || 4000;

const https = require('https');
const fs = require('fs');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}

const TokenService = require('/home/celia/node/auth-test/services/token.services');
const PassService = require('/home/celia/node/auth-test/services/pass.service');

const moment = require('moment');
const logger = require('morgan');
const express = require('express');
const res = require('express/lib/response');

const mongojs = require('mongojs');
const cors = require('cors');

const app = express();

//var db = mongojs('localhost:27017/SD-03'); 
var db = mongojs("SD");
var id = mongojs.ObjectId;
//var user = db.collection("user");
var auth = db.collection("auth");

// middlewares
/*var allowMethods = (req, res, next) => {
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    return next();
 };
    
var allowCrossTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "token");
    return next();
};*/

var allowCrossToTokenHeader = (req, res, next) => {
    res.header("Access-Control-Allow-Headers", "*");
    return next();
};
var allowCrossToTokenOrigin = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    return next();
};

app.use(logger('dev'));
app.use(express.json());
app.use(cors());
app.use(allowCrossToTokenHeader);
app.use(allowCrossToTokenOrigin);

var auth = (req, res, next) => {
    let token = req.headers.authorization.split(' ')[1];
    TokenService.decodificaToken( token ).then(user=>{
        req.user = {
            id: user.id
        }
        return next();
    }).catch( err => res.status(400).json({
        'result': 'KO',
        'err': err
    }));
}
//
/*app.param("user", (req, res, next, user) => {
    console.log('param /api/user');
    console.log('user: ', user);

    db.user = db.collection(user);
    return next();
});

// routes
app.param("user", (req, res, next, user) => {
    db.user = db.user(user);
    return next();
});*/

/*app.get('/api', (req, res, next) => {
    console.log('GET /api');
    console.log(req.params);
    console.log(db.user);

    db.getCollectionNames((err, user) => {
        if (err) return next(err);
        res.json(user);
    });
});*/
//user
app.get('/api/user', auth , (req, res, next) => {
    db.user.find((err, users)=>{
        if (err) return next(err);
        res.json(users);
    });
});

app.get('/api/user/:id', auth , (req, res, next) => {
    db.user.findOne({_id:id(req.params.id)}, (err, usuario) => {
        if (err) return next(err);
        res.json(usuario);
    });
});

app.post('/api/user', (req, res, next) => {
    const elemento = req.body;
    console.log('entro');

    if (!elemento.email || !elemento.password) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Faltan <email> y <password>'
        });
    }
    else {
        db.user.findOne({'email':elemento.email}, (err, usuario) => {
            if (usuario){
                res.json({
                'result':'KO',
                'err':'El email ya esta registrado'
                });
            }
            else{
                let usuario;
                PassService.encriptaPassword( elemento.password ).then( hash =>{
                    usuario = {
                        email : elemento.email,
                        password : hash,
                        fechaAlTa : moment().unix()
                    }
                    elemento.password = hash;
                    db.user.save(usuario, (err, usuarioGuardado) => {
                        if(err) return next({
                            'result': 'KO',
                            'err': err
                        });
                        else{
                            const token = TokenService.creaToken( usuarioGuardado );
                            res.json({
                                'result':'OK',
                                'token':token,
                                'usuario':usuarioGuardado
                            });
                        }
                    });
                });
            }
        });                
    }
});

app.put('/api/user/:id', auth, (req, res, next) => {
    let usuNuevo = req.body;
    
    PassService.encriptaPassword( usuNuevo.password ).then( hash =>{
        usuNuevo.password = hash;
        db.user.update({_id: id(req.params.id)}, 
            {$set: usuNuevo}, {safe :true, multi :false}, (err, userMod) =>{
            if(err) return next(err);
                res.json(userMod);
            });
    });
});

app.delete('/api/user/:id', auth,(req, res, next) => {
    db.user.remove({_id :id(req.params.id)}, (err, resultado) =>{
        if (err) next(err);
        res.json(resultado);
    });
});

// Iniciamos la aplicación
// app.listen(port, () => {
   // console.log(`API REST ejecutándose en http://localhost:${port}/api/:coleccion/:id`);
//});

//auth
//Obtenemos todos los usuarios registrados en el sistema. Mostramos versión
//reducida de GET /api/user.
app.get('/api/auth', auth, (req, res, next) => {
    db.user.find({},{
        'email':1,
        '_id':0
    },(err, user)=>{
        if (err) return next(err);
        res.json(user);
    });
});
//Obtenemos el usuario a partir de un token válido
app.get('/api/auth/me', auth, (req, res) => {
    db.user.findOne({_id:id(req.user.id)}, (err, usuario) => {
        if (err) return next(err);
        res.json({
            'result':'OK',
            'user': usuario
        });
    });                    
});
//Realiza una identificación o login (signIn) y devuelve un token válido.
app.post('/api/auth', (req, res, next) => { //login
    const elemento = req.body;

    if (!elemento.email || !elemento.password) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se necesitan los campos <email> y <password>'
        });
    }
    else {
        db.user.findOne({'email':elemento.email}, (err, usuario) => {
            if(usuario){
                PassService.comparaPassword(elemento.password, usuario.password).then(isOK =>{
                    if(isOK){
                        const token = TokenService.creaToken( usuario );
                        res.json({
                            'result':'OK',
                            'token':token
                        });
                    }
                    else{
                        res.json({
                            'result':'KO',
                            'err':'contraseña incorrecta'
                        });
                    }
                }).catch(err =>{
                    res.json({
                        'result':'KO',
                        'err':err});
                });
            }
            else{
                res.json({
                    'result':'KO',
                    'err':'El email no esta registrado'
                });
            }
        });    
    }
});
//Realiza un registro mínimo (signUp) de un usuario y devuelve un token válido.
app.post('/api/rec', (req, res, next) => {
    const elemento = req.body;

    if(!elemento.email || !elemento.password) {
        res.status(400).json ({
            error: 'Bad data',
            description: 'Se necesita <email> y <password>'
        });
    }
    else {
        db.user.findOne({'email':elemento.email}, (err, usuario) => {
            if (usuario){
                res.json({
                'result':'KO',
                'err':'Email ya registrado'
                });
            }
            else{
                let usuario;
                PassService.encriptaPassword( elemento.password ).then( hash =>{
                    usuario = {
                        email : elemento.email,
                        password : hash,
                        fechaAlTa : moment().unix()
                    }
                    elemento.password = hash;
                    db.user.save(usuario, (err, usuarioGuardado) => {
                        if(err) return next({
                            'result': 'KO',
                            'err': err
                        });
                        else{
                            const token = TokenService.creaToken( usuarioGuardado );
                            res.json({
                                'result':'OK',
                                'token':token,
                                'usuario':usuarioGuardado
                            });
                        }
                    });
                });
            }
        });                
    }
});
        
//el servicio
https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`Api-auth ejecutandose en https://localhost:${port}/api/auth`);
});